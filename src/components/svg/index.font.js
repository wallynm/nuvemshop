module.exports = {
  files: [
    './icons/*.svg'
  ],
  'fontName': 'icon',
  'classPrefix': 'icon-',
  'baseSelector': '.icon',
  'types': ['eot', 'woff', 'woff2', 'ttf', 'svg']
};