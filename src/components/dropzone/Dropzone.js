import React, { PureComponent } from 'react';
import classname from 'classname';
import ReactDropzone from 'react-dropzone';
import isEmpty from 'lodash/isEmpty';

import './Dropzone.scss';
class Dropzone extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      imageBase64: props.value
    }
  }

  componentWillReceiveProps(newProps) {
    this.setState({
      imageBase64: newProps.value
    })
  }

  onDropHandler = (files) => {
    const { onDrop , index} = this.props;
    
    var file = files[0]
    const reader = new FileReader();
    reader.onload = (event) => {
      const image = event.target.result;
      this.setState({imageBase64: image});

      if(onDrop && typeof onDrop === 'function') {
        onDrop(image, index);
      }
    };
    reader.readAsDataURL(file);
  }

  render() {
    const { imageBase64 } = this.state;
    const { className, onDrop, ...props } = this.props;
    const classComponent = classname('dropzone', className,{
      'dropzone--filled': (!isEmpty(imageBase64))
    });    
    let dropzoneRef;
    let style = {};
    
    if(imageBase64) {
      style = { backgroundImage: `url(${imageBase64}` };
    }

    return (
      <ReactDropzone className={classComponent} style={style}
        ref={(node) => { dropzoneRef = node; }} 
        // onClick={() => { dropzoneRef.open() }}
        onDrop={this.onDropHandler}
        {...props}
        >
        {!imageBase64 && <i className='icon icon-photo' />}
      </ReactDropzone>
    );
  }
}

export default Dropzone;