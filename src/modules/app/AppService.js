import Store from './AppStore';
import { action } from 'mobx';
import History from 'configs/routes/History';
import { Stores } from 'configs';
class AppService {
  @action
  toggleSidebar() {
    Store.sidebarOpen = !Store.sidebarOpen;
  }
}

export default new AppService();