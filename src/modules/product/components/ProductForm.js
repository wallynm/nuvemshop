import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import { withRouter } from 'react-router';
import isEmpty from 'lodash/isEmpty';
import './ProductForm.scss';

import { Grid } from 'components/grid';
import { Dropzone } from 'components/dropzone';
import { InputGroup, InputGroupIcon, Editor, InputGroupCurrencyIcon } from 'components/input';
import { Button } from 'components/button';
import { ProductService } from 'modules/product';
import { AppService } from 'modules/app';


@inject('RouterStore')
@withRouter
@observer
class ProductForm extends Component {
  state = {
    inputValidation: {
      name: false,
      description: false,
      price: false,
      stock: false
    },
    images: [],
    name: '',
    description: '',
    promotionalPrice: '',
    price: '',
    stock: ''
  };

  inputChange = e => {
    const { value, name } = e.target;
    this.onChange(name, value);
  }

  onChange = (name, value )=> {
    const state = this.state;
    state[name] = value;
    this.setState(state);
  }

  dropImage = (img, index) => {
    const state = this.state;

    if(typeof state.images[index] === 'undefined') {
      state.images.push(img);
    } else {
      state.images[index] = img;
    }

    this.setState(state);
  }

  backToListing = () => {
    this.props.history.push('/products') 
  }

  validateForm() {
    const state = this.state;
    const errors = [];
    let validate = true;

    for(var key in state.inputValidation) {
      if(state.inputValidation.hasOwnProperty(key)) {
        const invalid = (isEmpty(state[key]) || state[key] === '<p></p>');
    
        state.inputValidation[key] = invalid;

        if(invalid) {
          validate = false
        }
      }
    }

    this.setState(state);
    return validate;
  }

  saveHandler = () => {
    if(!this.validateForm()) {
      return null;
    }

    const state = this.state;
    if(state.id) {
      ProductService.updateProduct(state);
    } else {
      ProductService.addProduct(state);
    }
    this.backToListing();
  }

  removeHandler = () => {
    const { id } = this.state;
    if (id) {
      ProductService.removeProduct(id);
    }

    this.backToListing();
  }

  get renderActionButtons() {
    const { id } = this.state;
    if(id) {
      return (
        <React.Fragment>
          <Button size="small" onClick={this.saveHandler}>SALVAR ALTERAÇÕES</Button>
          <Button size="small" type="danger" onClick={this.removeHandler} className="ml--lg">APAGAR</Button>
          <Button size="small" onClick={this.backToListing} className="ml--lg" outline>CANCELAR</Button>
        </React.Fragment>
      )
    } else {
      return (
        <React.Fragment>
          <Button size="small" onClick={this.saveHandler}>PUBLICAR PRODUTO</Button>
          <Button size="small" onClick={this.backToListing} className="ml--lg" outline>CANCELAR</Button>
        </React.Fragment>
      )
    }
  }
  
  componentDidMount() {
    const { params } = this.props.match;
    if(params.id) {
      this.setState( ProductService.getProduct(params.id));
    }
  }

  render() {
    const { id, description, name, price, stock, promotionalPrice, images, inputValidation } = this.state;

    return (
      <div>
        <Grid transparent className='image--selection'>
          <label>Fotos dos seus produtos</label>
          <div className='col-1-4'>
            <Dropzone value={images[0]} index={0} onDrop={this.dropImage}/>
          </div>
          <div className='col-1-4'>
            <Dropzone value={images[1]} index={1} onDrop={this.dropImage}/>
          </div>
          <div className='col-1-4'>
            <Dropzone value={images[2]} index={2} onDrop={this.dropImage}/>
          </div>
          <div className='col-1-4'>
            <Dropzone value={images[3]} index={3} onDrop={this.dropImage}/>
          </div>
        </Grid>

        <Grid block>
          <div>
            <InputGroup value={name} validate={inputValidation} onChange={this.inputChange} label="Nome" name="name" placeholder="Ex: Chaveiro de plástico de Budha"/>
            <Editor value={description} validate={inputValidation} onChange={this.onChange} label="Descrição" name="description"/> 
          </div>

        </Grid>


        <Grid block>
        <div className="col-1-4">
            <InputGroupCurrencyIcon validate={inputValidation} value={price} onChange={this.inputChange} name="price" label="Preço Original" icon="R$" placeholder="0,00"/>
          </div>

          <div className="col-1-4">
            <InputGroupCurrencyIcon validate={inputValidation} value={promotionalPrice} onChange={this.inputChange} name="promotionalPrice" label="Preço Promocional" icon="R$" placeholder="0,00"/>
          </div>
          
          <div className="col-1-4">
            <InputGroup validate={inputValidation} value={stock} onChange={this.inputChange} type="number" name="stock" label="Estoque"/>
          </div>
        </Grid>

        <Grid transparent nopadding className="mt--lg">
          {this.renderActionButtons}
        </Grid>
      </div>
    )
  }
}

export default ProductForm;